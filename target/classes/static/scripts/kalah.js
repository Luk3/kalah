'use strict';

const BoardSides = {
		SOUTH: 'South',
		NORTH: 'North'
	}

const NorthHouse = 0;
const SouthHouse = 7;

function updateGame(game) {
	updateBoard(game.board);
	currentSide = game.currentSide;
	setTurn(currentSide);
}

function updateBoard(boardPieces) {
	$(".boardspot").each(function() {
		console.log("BoardPiece: "+ boardPieces[$(this).attr("value")] + " Value of this: " + $(this).attr("value"))
		$(this).html(boardPieces[$(this).attr("value")]);
	})
}

function setTurn(currentSide) {
	var boardSide = (currentSide == BoardSides.SOUTH) ? ".p2" : ".p1";

	$(".boardspot").css("background-color", "grey");
	
	$(boardSide).css("background-color", "red");
}