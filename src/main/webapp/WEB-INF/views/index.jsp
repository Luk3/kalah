<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Kalah</title>
        
        <script src="/webjars/jquery/jquery.min.js"></script>
        <script src="/webjars/sockjs-client/sockjs.min.js"></script>
        <script src="/webjars/stomp-websocket/stomp.min.js"></script>
        <script src="/scripts/kalah.js"></script>
        <script src="/scripts/app.js"></script>

        <link href="/css/main.css" rel="stylesheet">

        <script> 
            var currentSide = "${currentSide}";
            $(document).ready(init); 
        </script>
    </head>

    <body>
        <noscript>
            <h2 style="color: #ff0000">Javascript is required to run this page.  Please enable. </h2>
        </noscript>

        <div id="container">
            <div id="resetButton"> Reset Game </div>
            <div id="gameboard">
                <br/>
                <div class="boardspot home p1" value="0"> ${board.zero} </div>
                <div class="boardspot pit p1" value="1"> ${board.one} </div>
                <div class="boardspot pit p1" value="2"> ${board.two} </div>
                <div class="boardspot pit p1" value="3"> ${board.three} </div>
                <div class="boardspot pit p1" value="4"> ${board.four} </div>
                <div class="boardspot pit p1" value="5"> ${board.five} </div>
                <div class="boardspot pit p1" value="6"> ${board.six} </div>
                <div class="boardspot pit p2" value="13"> ${board.thirteen} </div>
                <div class="boardspot pit p2" value="12"> ${board.twelve} </div>
                <div class="boardspot pit p2" value="11"> ${board.eleven} </div>
                <div class="boardspot pit p2" value="10"> ${board.ten} </div>
                <div class="boardspot pit p2" value="9"> ${board.nine} </div>
                <div class="boardspot pit p2" value="8"> ${board.eight} </div>
                <div class="boardspot home upRight p2" value="7"> ${board.seven} </div>
            </div>

            <div id="description">

                <div id="logo"> <h1> Kalah <h1> </div>

                <div id="content">
                    <h3> Rules </h3>
                    The 1940's game of Kalah is played by two players. <br/><br/>

                    Each player chooses either the North or South squares.  On each turn, the player chooses one of his/her squares.  All of the numbers are immediatly consumed from the chosen square and distributed one by one to each of the following squares, counter-clockwise.  The squares highlighted in red indicate the current turn.<br/><br/>

                    Should the last number land on either "home" (the left big square for North, right for South), that player goes again.  Should the last number land on a square with zero numbers that is owned by that player, the player gets those numbers <i>plus</i> the numbers opposite of his/her square. <br/><br/>

                    Numbers are collected in each home for both North and South.  The game ends once either side is fully out of numbers.  At that time, remaining numbers are collected to the home of their respective side and the player with the highest score wins. <br/><br/>

                    <h3> About </h3>
                    One game is ran on the server.  The current game state is served when the website is first reached.  Any moves made by other players are sent to every other client...  So please don't click 'Reset Game' should a game be in action as it will reset it for whoever else is on the site.  <br/><br/>

                    But do use 'Reset Game' to reset the game board once the game comes to an end and all the numbers are collected in the two houses.  The side that goes first is random and will be highlighted in red. <br/><br/>

                    The game works on mobile as well...  Tested on Chrome, Firefox, and a lesser known browser.  <br/><br/>

                    <span style="font-size: 8pt"> *If you're on Firefox, there is an inconsistency in lining up the South's home.  18px lines it up in Chrome and others while 20px in Firefox...  Not sure about Safari.  So best viewing for this would be Chrome.</span>
                </div>
            </div>
        </div>
    </body>
</html>