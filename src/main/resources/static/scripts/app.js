"use strict";

var stompClient = null;

function init() {
	connect();

	setEvents();

	setTurn(currentSide);
}

// Connect to server
function connect() {
	var socket = new SockJS('/kalahConnect');

	stompClient = Stomp.over(socket);

	stompClient.connect({}, function(frame) {
		console.log("Connected: " + frame);
		setConnections();
	});
}

function getCurrentPlayer() {
	$.get("/getCurrentPlayer", function(data, status) {
		console.log(data);
	});
}

function setConnections() {
	stompClient.subscribe('/game/update', function(gameState) {
		updateGame(JSON.parse(gameState.body));
	});

	stompClient.subscribe('/game/reset', function(gameState) {
		updateGame(JSON.parse(gameState.body));
	});
}

function resetGame() {
	stompClient.send("/app/reset", {}, {});
}

function sendMove(event) {
	if (($(event.target).attr('value') < SouthHouse && currentSide != BoardSides.NORTH)
			|| ($(event.target).attr('value') > SouthHouse && currentSide != BoardSides.SOUTH)
			|| ($(event.target).html() == 0))
		return;

	var boardSpace = $(event.target).attr('value');

	stompClient.send("/app/move", {}, JSON.stringify({boardSpace: boardSpace, boardSide: currentSide}));
}

function setEvents() {
	$(".pit").on('click', sendMove);

	$("#resetButton").on('click', resetGame);
}

