package kalah.model;

import java.util.HashMap;
import java.util.HashSet;

public class GameState {

	private String currentSide;

	private HashMap<Integer, Integer> boardState;

	public GameState() {
		boardState = new HashMap<Integer, Integer>();
		currentSide = null; 
	}

	public GameState getGameState() {
		return null;
	}

	public void updateGame(HashMap<Integer, Integer> hashMap, String side) {
		boardState = hashMap; 
		currentSide = side;
	}

	public void updateBoard(HashMap<Integer, Integer> board) {
		boardState = board;
	}

	public void updateSide(String side) {
		currentSide = side;
	}
}