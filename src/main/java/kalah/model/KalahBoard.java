package kalah.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class KalahBoard {

	// Number of spaces on the board
		// 0 is home of Player 1, [1, 6] is Player1's pits.  
		// 7 is home of Player 2, [7, 12] is Player2's pits. 
	private final int[] BoardSpaces = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
	
	// Specific spaces
	private final int NorthHouse = 0;
	private final int SouthHouse = 7;
	private final int TotalSpaces = 13;

	// The houses
	private final HashSet<Integer> Houses = new HashSet<Integer>(Arrays.asList(BoardSpaces[NorthHouse], BoardSpaces[SouthHouse]));

	// Default starting stones for each pit. 
	private final int DefaultStartingStones = 6;
	private int totalStones; 

	private final int PitsInPlay = 6;

	// Board game
	private HashMap<Integer, Integer> board;
	
	// Game State
	public enum BoardState {INPLAY, GAMEOVER};
	private BoardState boardState = BoardState.INPLAY;


	public KalahBoard() {
		createBoard(DefaultStartingStones);
		totalStones = DefaultStartingStones*PitsInPlay;
	}

	public KalahBoard(int startingStones) {
		createBoard(startingStones);
		totalStones = startingStones*PitsInPlay;
	}

	private void createBoard(int startingStones) {
		board = new HashMap<Integer, Integer>();
		
		for (int space: BoardSpaces) 
			board.put(space, (Houses.contains(space) ? 0 : startingStones));	
	}

	public boolean makeMove(int boardSpace) {
		int stones = board.get(boardSpace);
		board.put(boardSpace, 0);

		int avoid;

		avoid = (boardSpace < SouthHouse) ? SouthHouse : NorthHouse;

		for (int i=0; i<stones; i++) {
			boardSpace--;
			if (boardSpace == -1)
				boardSpace = TotalSpaces;
			
			if (boardSpace != avoid) {
				if (i != stones-1)
					board.put(boardSpace, board.get(boardSpace)+1);
				else
					placeStoneOrSteal(boardSpace, avoid);
			}
			else
				stones++;
		}
				
		if (Houses.contains(boardSpace))
			if (boardSpace != avoid)
				return true;

		return false;
	}
	
	private void placeStoneOrSteal(int boardSpace, int avoidSpace) {
		int stonesInPlace = board.get(boardSpace);
		
		boolean steal = false;

		if (stonesInPlace == 0 && !Houses.contains(boardSpace)) 
			if (avoidSpace == SouthHouse && boardSpace < SouthHouse) // It's player North
				steal=true;
			else if (avoidSpace == NorthHouse && boardSpace > NorthHouse)
				steal=true;
		
		if (steal) {
			int space = getOppositeSpace(boardSpace);
			board.put(Math.abs(avoidSpace-SouthHouse), board.get(Math.abs(avoidSpace-SouthHouse)) + board.get(space) + 1);
			board.put(space, 0);
		}
		else
			board.put(boardSpace, board.get(boardSpace)+1);
	}
	
	private int getOppositeSpace(int boardSpace) {
		return board.size()-(boardSpace);
	}
	
	private void checkGameState() {
		int northSide, southSide;
		northSide = southSide = 0;
		
		for (Integer boardSpace: board.keySet()) {
			if (boardSpace > NorthHouse && boardSpace < SouthHouse) {
				if (board.get(boardSpace) == 0) 
					northSide++;
			}
			else if (boardSpace > SouthHouse && boardSpace < (TotalSpaces+1))
				if (board.get(boardSpace) == 0)
					southSide++;
		}
		
		if (northSide == PitsInPlay || southSide == PitsInPlay)
			boardState = BoardState.GAMEOVER;	
	}
	
	private void calculateFinalBoard() {
		for (Integer boardSpace: board.keySet()) {
			if (boardSpace > NorthHouse && boardSpace < SouthHouse) {
				if (board.get(boardSpace) > NorthHouse) {
					board.put(NorthHouse, board.get(NorthHouse) + board.get(boardSpace));
					board.put(boardSpace, 0);
				}
			}
			else if (boardSpace > SouthHouse && boardSpace < (TotalSpaces+1))
				if (board.get(boardSpace) > 0) {
					board.put(SouthHouse, board.get(SouthHouse) + board.get(boardSpace));
					board.put(boardSpace, 0);
				}
		}
	}
	
	public HashMap<Integer, Integer> getBoardState() {
		checkGameState();
		if (boardState == BoardState.GAMEOVER) 
			calculateFinalBoard();
		
		return board;
	}
	
	/*
	 * Result of having a HashMap with int keys when needing to
	 * access these values in compiling a jsp page...
	 * 
	 * Better solution is to simply send the data and let the client compile the page.
	 * Compiling a long HTML string to insert on page is another option...
	 */
	public HashMap<String, Integer> getBoardStateJSP() {
		HashMap<String, Integer> tempBoard = new HashMap<String, Integer>();
		
		tempBoard.put("zero", board.get(0));
		tempBoard.put("one", board.get(1));
		tempBoard.put("two", board.get(2));
		tempBoard.put("three", board.get(3));
		tempBoard.put("four", board.get(4));
		tempBoard.put("five", board.get(5));
		tempBoard.put("six", board.get(6));
		tempBoard.put("seven", board.get(7));
		tempBoard.put("eight", board.get(8));
		tempBoard.put("nine", board.get(9));
		tempBoard.put("ten", board.get(10));
		tempBoard.put("eleven", board.get(11));
		tempBoard.put("twelve", board.get(12));
		tempBoard.put("thirteen", board.get(13));
		
		return tempBoard;
	}
	
	public String getGameState() {
		return boardState.toString();
	}
	
	public void resetBoard() {
		createBoard(DefaultStartingStones);
		boardState = BoardState.INPLAY;
	}

}