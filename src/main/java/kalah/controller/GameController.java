package kalah.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.json.simple.JSONObject;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.messaging.simp.SimpMessageHeaderAccessor;

import kalah.model.GameState;
import kalah.model.KalahBoard;
import kalah.model.Player;

@Controller
public class GameController {

	private HashMap<String, Player> players = new HashMap<String, Player>();

	private Random random = new Random();

	public enum PlayerSide {North, South;};
	private PlayerSide playerSide = PlayerSide.South;

	private KalahBoard board = new KalahBoard();
	private GameState gameState = new GameState();
	
	/*
	 * Serves starting index.
	 * Sends current board state to compile on server. 
	*/
	@RequestMapping("/")
	public ModelAndView SendIndex() {
		
		ModelAndView modelView = new ModelAndView("index");
		modelView.addObject("board", board.getBoardStateJSP());
		modelView.addObject("currentSide", playerSide.toString());
		
		return modelView;
	}

	/*
	 * TODO:
	 * 	Implement session management for multiple game boards. 
	 * 	- More robust user/session management.
	 */
	@MessageMapping("/setPlayer") 
	public void setPlayer(String side, SimpMessageHeaderAccessor headerAccessor) throws Exception {
		
		players.put(headerAccessor.getSessionId(), new Player(headerAccessor.getSessionId()));
	}

	@MessageMapping("/reset") 
	@SendTo("/game/reset")
	public JSONObject resetGame(String boardSide, SimpMessageHeaderAccessor headerAccessor) throws Exception {
		
		board.resetBoard();

		playerSide = (random.nextBoolean()==true) ? PlayerSide.North : PlayerSide.South; 

		return getJSON(board.getBoardState(), playerSide.toString());
	}

	@MessageMapping("/move") 
	@SendTo("/game/update")	
	public JSONObject calculateMove(String boardSide, SimpMessageHeaderAccessor headerAccessor) throws Exception {

		if (board.getGameState() == "INPLAY") 
			makeMove(boardSide);
						
		return getJSON(board.getBoardState(), playerSide.toString());
	}
	
	private void makeMove(String boardSide) throws Exception {

		Map playerData = getMap(boardSide);
		
		boolean goAgain = false;
				
		int boardSpace = Integer.parseInt(playerData.get("boardSpace").toString());
				
		if (playerSide.toString().equals(playerData.get("boardSide")))
			goAgain = board.makeMove(boardSpace);


		if (!goAgain)
			playerSide = (playerSide == PlayerSide.North) ? PlayerSide.South : PlayerSide.North;
	}

	private JSONObject getJSON(HashMap<Integer, Integer> currentBoard, String currentSide) {

		JSONObject json = new JSONObject();
		json.put("board", currentBoard);
		json.put("currentSide", currentSide);

		return json;
	}

	private Map getMap(String boardSide) throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();

		return mapper.readValue(boardSide, Map.class);
	}
}