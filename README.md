# README #

Kalah...  A simple implementation. 

### About ###

The game of Kalah in a web browser.

The back-end is written in Java using Spring.  The front is JS using jQuery.  

Communication is established and maintained between the client and server via SockJS allowing for a constant two-way connection.  SockJS has bult-in fallback capabilities should sockets not be supported on the client such as long polling. 

On first connection the client is served a pre-compiled JSP page from the server.  Following that, all interactions take place via sockets and the view is updated via JavaScript. 

A single game is run on the server that anyone can jump into once connecting.  The game is primarily maintained by GameController.  Due to there being only three routes (including the initial page serve) the GameController also serves as a router.  However, should this project move forward it would be advised to create a separate class for routing. 

As for the game itself, it is stored in a KalahBoard object.  GameController makes references to KalahBoard as it used primarily as a Model.  There is an additional Player model and GameState model.  Should any further progress be made it is advised that GameState plays a bigger role in maintaining the game itself.  Player is intended to be used in managing sessions and keeping score should those features be implemented. 

src/main/java/kalah/ 

* config/WebSocketConfig.java 

* controller/GameController.java 

* model/GameState.java 

* model/KalahBoard.java 

* model/Player.java 

Main.java 

src/main/resources/static/ 

* css/main.css 

* scripts/app.js 

* scripts/kalah.js 

src/main/webapp/WEB-INF/ 

* views/index.jsp 


### To Run ###
Easiest way to run is as a Spring application.  

Instructions for Eclipse:  After importing the project run as 'Java Application' selecting 'Main - kalah'.  Then visit 'localhost:8080' in a browser.  Open in a separate browser to simulate another user or open on another system's browser connected to the network the application is hosted on.


### Post Mortem ###
There is always a question of compiling on the server or on the client.  For this project there is only one page and it is pre-compiled on the server as a jsp page.  The only data that needed to be inserted is the current board layout but it proved more cumbersome than necessary due to the board using integer's as a key and jsp not liking ints as keys.